package com.greedygame.messageviewer.config;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.greedygame.messageviewer.model.dao.DaoMaster;
import com.greedygame.messageviewer.model.dao.DaoSession;
import com.greedygame.messageviewer.util.PrefUtils;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class MessageViewerApplication extends Application {

    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        PrefUtils.init(this);

        DaoMaster.DevOpenHelper messageViewHelper = new DaoMaster.DevOpenHelper(this, "MessageViewer", null);
        SQLiteDatabase messageViewerDatabase = messageViewHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(messageViewerDatabase);
        mDaoSession = daoMaster.newSession();


    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}
