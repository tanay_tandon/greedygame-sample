package com.greedygame.messageviewer.config.constants;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class Code {

    public static final int READ_SMS_PERMISSION_CODE = 1;
    public static final int READ_CONTACT_PERMISSION_COOE = 2;
    public static final int READ_CONTACT_AND_SMS_PERMISSION_CODE = 3;
}
