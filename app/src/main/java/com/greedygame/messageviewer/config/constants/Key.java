package com.greedygame.messageviewer.config.constants;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class Key {

    public static final String APP_PREFS_NAME = "message_prefs";
    public static final String SPAM_MESSAGE_SET = "spam_message_set";

    public static final String SPAM_MESSAGE_COUNT = "spam_message_count";

    public static final String MESSAGE_ID = "_id";
    public static final String MESSAGE_SENDER = "address";
    public static final String MESSAGE_TIMESTAMP = "date";
    public static final String MESSAGE_BODY = "body";
}
