package com.greedygame.messageviewer.config.constants;

/**
 * Created by tanaytandon on 13/01/16.
 */
public class Type {

    public enum  ViewType {
        DATE,
        MESSAGE
    };
}
