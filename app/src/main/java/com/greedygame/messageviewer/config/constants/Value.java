package com.greedygame.messageviewer.config.constants;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class Value {

    public static final String READ_MESSAGE_URI = "content://sms/inbox";
    public static final String DATE_FORMAT = "dd--MM--yyyy";
}
