package com.greedygame.messageviewer.controller;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;

import com.greedygame.messageviewer.listener.MessageDeletionListener;
import com.greedygame.messageviewer.listener.MessageLoaderListener;
import com.greedygame.messageviewer.model.MessageBaseModel;
import com.greedygame.messageviewer.task.MarkMessageAsSpamTask;
import com.greedygame.messageviewer.task.MessageDeletionTask;
import com.greedygame.messageviewer.task.MessageLoaderTask;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 13/01/16.
 */
public class MessageController {

    public void loadMessageAsync(Activity activity, boolean canReadContacts,
                                 MessageLoaderListener messageLoaderListener) {
        MessageLoaderTask messageLoaderTask = new MessageLoaderTask(activity, canReadContacts, messageLoaderListener);
        messageLoaderTask.execute();
    }

    public void deleteMessageAsync(Activity activity, MessageDeletionListener messageDeletionListener, ArrayList<MessageBaseModel> dataset) {
        MessageDeletionTask messageDeletionTask = new MessageDeletionTask(activity, dataset, messageDeletionListener);
        messageDeletionTask.execute();
    }

    public void toggleMessageAsSpamAsync(Context context, String id) {
        MarkMessageAsSpamTask markMessageAsSpamTask = new MarkMessageAsSpamTask(context, id);
        markMessageAsSpamTask.execute();
    }
}
