package com.greedygame.messageviewer.listener;

import com.greedygame.messageviewer.model.MessageBaseModel;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 14/01/16.
 */
public interface MessageDeletionListener {

    public void preMessageDeletion();

    public void postMessageDeletion();
}
