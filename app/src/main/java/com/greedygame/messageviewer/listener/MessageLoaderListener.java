package com.greedygame.messageviewer.listener;

import com.greedygame.messageviewer.model.MessageBaseModel;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 14/01/16.
 */
public interface MessageLoaderListener {

    public void onPreLoad();

    public void onPostLoad(ArrayList<MessageBaseModel> dataset);
}
