package com.greedygame.messageviewer.model;

import com.greedygame.messageviewer.config.constants.Type;

/**
 * Created by tanaytandon on 13/01/16.
 */
public class MessageContent extends MessageBaseModel {

    public String content;
    public String sender;
    public String time;
    public String id;
    public boolean isSpam;

    public MessageContent(String id, String content, String sender, String time, boolean isSpam) {
        this.id = id;
        this.content = content;
        this.sender = sender;
        this.time = time;
        this.isSpam = isSpam;
        this.mViewType = Type.ViewType.MESSAGE;
    }


}
