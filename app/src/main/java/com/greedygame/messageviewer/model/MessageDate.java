package com.greedygame.messageviewer.model;

import com.greedygame.messageviewer.config.constants.Type;

/**
 * Created by tanaytandon on 13/01/16.
 */
public class MessageDate extends MessageBaseModel {

    public String date;

    public MessageDate(String date) {
        this.date = date;
        this.mViewType = Type.ViewType.DATE;
    }
}
