package com.greedygame.messageviewer.task;

import android.content.Context;
import android.os.AsyncTask;

import com.greedygame.messageviewer.config.MessageViewerApplication;
import com.greedygame.messageviewer.model.dao.Message;
import com.greedygame.messageviewer.model.dao.MessageDao;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 15/01/16.
 */
public class MarkMessageAsSpamTask extends AsyncTask<Void, Void, Void> {

    private MessageViewerApplication messageViewerApplication;
    private String messageId;

    public MarkMessageAsSpamTask(Context context, String messageId) {
        this.messageViewerApplication = (MessageViewerApplication) context.getApplicationContext();
        this.messageId = messageId;
    }

    @Override
    protected Void doInBackground(Void... params) {
        Long id = Long.parseLong(messageId);
        MessageDao messageDao = messageViewerApplication.getDaoSession().getMessageDao();
        ArrayList<Message> messages = (ArrayList<Message>) messageDao.queryBuilder().where(MessageDao.Properties.Id.eq(id)).limit(1).list();
        Message message = null;
        if(messages.size() != 0) {
            message = messages.get(0);
            message.setIsSpam(!message.getIsSpam());
        } else {
            message = new Message(id, false, true);
        }
        messageDao.insertOrReplace(message);
        return null;
    }
}
