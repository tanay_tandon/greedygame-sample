package com.greedygame.messageviewer.task;

import android.app.Activity;
import android.os.AsyncTask;

import com.greedygame.messageviewer.config.MessageViewerApplication;
import com.greedygame.messageviewer.config.constants.Type;
import com.greedygame.messageviewer.listener.MessageDeletionListener;
import com.greedygame.messageviewer.model.MessageBaseModel;
import com.greedygame.messageviewer.model.MessageContent;
import com.greedygame.messageviewer.model.dao.Message;
import com.greedygame.messageviewer.model.dao.MessageDao;

import java.util.ArrayList;

public class MessageDeletionTask extends AsyncTask<Void, Void, Void> {

    private MessageViewerApplication messageViewerApplication;
    private ArrayList<MessageBaseModel> dataset;
    private MessageDeletionListener messageDeletionListener;

    public MessageDeletionTask(Activity activity, ArrayList<MessageBaseModel> dataset, MessageDeletionListener messageDeletionListener) {
        this.messageViewerApplication = (MessageViewerApplication) activity.getApplication();
        this.dataset = dataset;
        this.messageDeletionListener = messageDeletionListener;
    }


    @Override
    protected void onPreExecute() {
        messageDeletionListener.preMessageDeletion();
    }

    @Override
    protected Void doInBackground(Void... params) {
        ArrayList<MessageBaseModel> newDataset = new ArrayList<>();
        MessageDao messageDao = messageViewerApplication.getDaoSession().getMessageDao();
        for (MessageBaseModel messageBaseModel : dataset) {
            if (messageBaseModel.mViewType == Type.ViewType.MESSAGE) {
                MessageContent messageContent = (MessageContent) messageBaseModel;
                if (messageContent.isSpam) {
                    ArrayList<Message> messages = (ArrayList<Message>) messageDao.queryBuilder().
                            where(MessageDao.Properties.Id.eq(Long.parseLong(messageContent.id))).list();
                    if(messages.size() != 0) {
                        Message message = messages.get(0);
                        message.setIsDeleted(true);
                        messageDao.insertOrReplace(message);
                    }
                } else {
                    newDataset.add(messageBaseModel);
                }
            } else if(messageBaseModel.mViewType == Type.ViewType.DATE) {
                int size = newDataset.size();
                if(size != 0 && newDataset.get(size - 1).mViewType == Type.ViewType.DATE) {
                    newDataset.remove(size - 1);
                }
                newDataset.add(messageBaseModel);
            }
        }
        this.dataset.clear();
        this.dataset.addAll(newDataset);
        return null;
    }

    @Override
    protected void onPostExecute(Void parmas) {
        this.messageDeletionListener.postMessageDeletion();
    }

}
