package com.greedygame.messageviewer.task;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import com.greedygame.messageviewer.config.MessageViewerApplication;
import com.greedygame.messageviewer.config.constants.Key;
import com.greedygame.messageviewer.config.constants.Value;
import com.greedygame.messageviewer.listener.MessageLoaderListener;
import com.greedygame.messageviewer.model.MessageBaseModel;
import com.greedygame.messageviewer.model.MessageContent;
import com.greedygame.messageviewer.model.MessageDate;
import com.greedygame.messageviewer.model.dao.Message;
import com.greedygame.messageviewer.model.dao.MessageDao;
import com.greedygame.messageviewer.util.ContactUtils;
import com.greedygame.messageviewer.util.TimeUtils;
import com.greedygame.messageviewer.view.adapter.MessageAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageLoaderTask extends AsyncTask<Void, Void, ArrayList<MessageBaseModel>> {

    private MessageLoaderListener messageLoaderListener;
    private ContentResolver contentResolver;
    private MessageViewerApplication messageViewerApplication;
    private boolean canViewContacts;
    private HashMap<String, String> hmNumberToName;
    public static final String TAG = MessageLoaderTask.class.getName();

    public MessageLoaderTask(Activity activity, boolean canViewContacts, MessageLoaderListener messageLoaderListener) {
        this.contentResolver = activity.getContentResolver();
        this.messageViewerApplication = (MessageViewerApplication) activity.getApplication();
        this.canViewContacts = canViewContacts;
        this.messageLoaderListener = messageLoaderListener;
    }

    private String getContactNameFromNumber(String contactNumber) {
        if (!canViewContacts) {
            return contactNumber;
        }
        if (hmNumberToName.containsKey(contactNumber)) {
            return hmNumberToName.get(contactNumber);
        } else {
            String contactName = ContactUtils.getContactNameFromNumber(this.contentResolver, contactNumber);
            hmNumberToName.put(contactNumber, contactName);
            return contactName;
        }
    }

    private ArrayList<Long> getListOfDeletedMessages() {
        ArrayList<Long> listOfDeletedMessages = new ArrayList<>();
        MessageDao messageDao = messageViewerApplication.getDaoSession().getMessageDao();
        ArrayList<Message> messages = (ArrayList<Message>) messageDao.queryBuilder().where(MessageDao.Properties.IsDeleted.eq(true)).list();
        for (Message message : messages) {
            listOfDeletedMessages.add(message.getId());
        }
        return listOfDeletedMessages;
    }


    private ArrayList<Long> getListOfSpamMessages() {
        ArrayList<Long> listOfSpamMessages = new ArrayList<>();
        MessageDao messageDao = messageViewerApplication.getDaoSession().getMessageDao();
        ArrayList<Message> messages = (ArrayList<Message>) messageDao.queryBuilder().where(MessageDao.Properties.IsSpam.eq(true), MessageDao.Properties.IsDeleted.eq(false)).list();
        for (Message message : messages) {
            listOfSpamMessages.add(message.getId());
        }
        MessageAdapter.MESSAGES_MARKED_AS_SPAM_COUNT = listOfSpamMessages.size();
        return listOfSpamMessages;
    }


    @Override
    protected void onPreExecute() {
        messageLoaderListener.onPreLoad();
    }

    @Override
    protected ArrayList<MessageBaseModel> doInBackground(Void... params) {
        this.hmNumberToName = new HashMap<>();
        ArrayList<MessageBaseModel> dataset = new ArrayList<>();
        Uri messageUri = Uri.parse(Value.READ_MESSAGE_URI);
        Cursor cursor = this.contentResolver.query(messageUri,
                new String[]{Key.MESSAGE_ID, Key.MESSAGE_SENDER, Key.MESSAGE_TIMESTAMP, Key.MESSAGE_BODY}, null,
                null, Key.MESSAGE_TIMESTAMP + " DESC");
        if (cursor != null) {
            cursor.moveToFirst();
            String currentDate = "";
            ArrayList<Long> listOfDeletedMessages = getListOfDeletedMessages();
            ArrayList<Long> listOfSpamMessages = getListOfSpamMessages();
            while (cursor.moveToNext()) {
                String messageId = cursor.getString(cursor.getColumnIndex(Key.MESSAGE_ID));
                Long id = Long.parseLong(messageId);
                if (!listOfDeletedMessages.contains(id)) {

                    Long messageTimeStamp = Long.parseLong(cursor.getString(cursor.getColumnIndex(Key.MESSAGE_TIMESTAMP)));
                    MessageContent messageContent = new MessageContent(messageId, cursor.getString(cursor.getColumnIndex(Key.MESSAGE_BODY)),
                            getContactNameFromNumber(cursor.getString(cursor.getColumnIndex(Key.MESSAGE_SENDER))),
                            TimeUtils.getTime(messageTimeStamp), listOfSpamMessages.contains(id));
                    String messageDate = TimeUtils.getDate(messageTimeStamp);
                    if (!messageDate.equalsIgnoreCase(currentDate)) {
                        dataset.add(new MessageDate(messageDate));
                        currentDate = messageDate;
                    }
                    dataset.add(messageContent);
                }

            }
            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        return dataset;
    }

    @Override
    protected void onPostExecute(ArrayList<MessageBaseModel> dataset) {
        messageLoaderListener.onPostLoad(dataset);
    }
}
