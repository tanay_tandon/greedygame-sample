package com.greedygame.messageviewer.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

/**
 * Created by tanaytandon on 15/01/16.
 */
public class ContactUtils {

    public static String getContactNameFromNumber(ContentResolver contentResolver, String contactNumber) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(contactNumber));
        Cursor cursor = contentResolver.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return contactNumber;
        }
        String contactName = null;

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(contactName == null || contactName.length() == 0) {
            return contactNumber;
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }

        return contactName;

    }
}
