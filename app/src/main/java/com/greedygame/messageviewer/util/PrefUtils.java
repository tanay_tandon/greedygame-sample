package com.greedygame.messageviewer.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.greedygame.messageviewer.config.constants.Key;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class PrefUtils {

    private static SharedPreferences mSharedPreferences;
    private static SharedPreferences.Editor mEditor;

    public static void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(Key.APP_PREFS_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
        mEditor.commit();
    }

    public static void setMessageAsSpam(String messageId) {
        Set<String> spamMessageSet = mSharedPreferences.getStringSet(Key.SPAM_MESSAGE_SET, null);
        if (spamMessageSet == null) {
            spamMessageSet = new HashSet<>();
        }
        spamMessageSet.add(messageId);
        mEditor.putStringSet(Key.SPAM_MESSAGE_SET, spamMessageSet).commit();
    }

    public static void removeMessageAsSpam(String messageId) {
        Set<String> spamMessageSet = mSharedPreferences.getStringSet(Key.SPAM_MESSAGE_SET, null);
        if (spamMessageSet != null) {
            spamMessageSet.remove(messageId);
            mEditor.putStringSet(Key.SPAM_MESSAGE_SET, spamMessageSet).commit();
        }
    }

    public static HashSet<String> getSpamMessageSet() {
        return (HashSet<String>) mSharedPreferences.getStringSet(Key.SPAM_MESSAGE_SET, new HashSet<String>());
    }
}
