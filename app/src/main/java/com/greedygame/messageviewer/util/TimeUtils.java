package com.greedygame.messageviewer.util;

import android.text.format.DateFormat;

import com.greedygame.messageviewer.config.constants.Value;

import java.util.Calendar;

public class TimeUtils {

    public static String getDate(long time) {
        Calendar calendarInstance = Calendar.getInstance();
        calendarInstance.setTimeInMillis(time);
        return DateFormat.format(Value.DATE_FORMAT, calendarInstance).toString();
    }

    public static String getTime(long time) {
        Calendar calendarInstance = Calendar.getInstance();
        calendarInstance.setTimeInMillis(time);
        return java.text.DateFormat.getTimeInstance().format(calendarInstance.getTime());
    }
}
