package com.greedygame.messageviewer.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.Manifest;

import com.greedygame.messageviewer.R;
import com.greedygame.messageviewer.config.constants.Code;
import com.greedygame.messageviewer.controller.MessageController;
import com.greedygame.messageviewer.listener.MessageDeletionListener;
import com.greedygame.messageviewer.listener.MessageLoaderListener;
import com.greedygame.messageviewer.model.MessageBaseModel;
import com.greedygame.messageviewer.model.dao.Message;
import com.greedygame.messageviewer.view.adapter.MessageAdapter;
import com.greedygame.messageviewer.view.fragment.DeletionConfirmationDialog;

import java.util.ArrayList;


public class MessageActivity extends AppCompatActivity implements MessageLoaderListener, MessageDeletionListener {

    private View clRoot, pbLoadingMessages;
    private MessageController messageController;
    private MessageAdapter messageAdapter;
    private ArrayList<MessageBaseModel> dataset;
    private boolean loadingMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.messageController = new MessageController();
        this.clRoot = findViewById(R.id.cl_root);
        this.pbLoadingMessages = findViewById(R.id.pb_loading_messages);
        this.dataset = new ArrayList<>();
        this.loadingMessages = true;
        // ask for  permission if version is Marshmallow
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermission(Manifest.permission.READ_SMS)) {
                requestPermission(new String[]{Manifest.permission.READ_SMS},
                        Code.READ_SMS_PERMISSION_CODE);
            }
        } else {
            loadRecyclerView();
        }

        Snackbar.make(clRoot, R.string.waiting_message, Snackbar.LENGTH_SHORT).show();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!loadingMessages) {
                    if(MessageAdapter.MESSAGES_MARKED_AS_SPAM_COUNT != 0) {
                        DeletionConfirmationDialog deletionConfirmationDialog = DeletionConfirmationDialog.newInstance();
                        deletionConfirmationDialog.show(getSupportFragmentManager(), "deletion confirmation");

                    } else {
                        Snackbar.make(clRoot, R.string.no_spam_messages, Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Snackbar.make(clRoot, R.string.waiting_message, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(brMessageDeletion, new IntentFilter(getResources().getString(R.string.intent_filter_deletion_broadcast)));
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(brMessageDeletion);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Code.READ_SMS_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(!hasPermission(Manifest.permission.READ_CONTACTS)) {
                        requestPermission(new String[]{Manifest.permission.READ_CONTACTS}, Code.READ_CONTACT_PERMISSION_COOE);
                    }
                } else {
                    findViewById(R.id.tv_allow_sms_read_permission).setVisibility(View.VISIBLE);
                }
                break;
            }

            case Code.READ_CONTACT_PERMISSION_COOE: {
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Snackbar.make(clRoot, R.string.contact_permission_denied, Snackbar.LENGTH_SHORT).show();
                }
                loadRecyclerView();
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadRecyclerView() {
        RecyclerView rvMessageList = (RecyclerView) findViewById(R.id.rv_message_list);

        rvMessageList.setHasFixedSize(true);

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        rvMessageList.setLayoutManager(mLinearLayoutManager);

        this.messageAdapter = new MessageAdapter(this, this.dataset);
        rvMessageList.setAdapter(this.messageAdapter);

        this.messageController.loadMessageAsync(this, hasPermission(Manifest.permission.READ_CONTACTS), this);
    }


    private boolean hasPermission(String permission) {
        int result = ContextCompat.checkSelfPermission(this, permission);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    private BroadcastReceiver brMessageDeletion = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            messageController.deleteMessageAsync(MessageActivity.this, MessageActivity.this, dataset);
        }
    };

    @Override
    public void onPreLoad() {
        this.loadingMessages = true;
        pbLoadingMessages.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPostLoad(ArrayList<MessageBaseModel> dataset) {
        this.loadingMessages = false;
        this.dataset.addAll(dataset);
        this.messageAdapter.notifyDataSetChanged();
        pbLoadingMessages.setVisibility(View.GONE);
    }

    @Override
    public void preMessageDeletion() {
        this.loadingMessages = true;
        pbLoadingMessages.setVisibility(View.VISIBLE);
        Snackbar.make(clRoot, R.string.deleting_messages, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void postMessageDeletion() {
        this.loadingMessages = false;
        this.messageAdapter.notifyDataSetChanged();
        pbLoadingMessages.setVisibility(View.GONE);
        MessageAdapter.MESSAGES_MARKED_AS_SPAM_COUNT = 0;
    }
}
