package com.greedygame.messageviewer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.greedygame.messageviewer.R;
import com.greedygame.messageviewer.config.constants.Type;
import com.greedygame.messageviewer.controller.MessageController;
import com.greedygame.messageviewer.model.MessageBaseModel;
import com.greedygame.messageviewer.model.MessageContent;
import com.greedygame.messageviewer.model.MessageDate;
import com.greedygame.messageviewer.view.viewholder.DateViewHolder;
import com.greedygame.messageviewer.view.viewholder.MessageViewHolder;

import java.util.ArrayList;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private Context mContext;
    private ArrayList<MessageBaseModel> dataset;
    private MessageController messageController;

    public static int MESSAGES_MARKED_AS_SPAM_COUNT = 0;

    public MessageAdapter(Context context, ArrayList<MessageBaseModel> dataset) {
        this.mContext = context;
        this.dataset = dataset;
        this.messageController = new MessageController();
    }

    @Override
    public int getItemViewType(int position) {
        Type.ViewType viewType = dataset.get(position).mViewType;
        if (viewType == Type.ViewType.DATE) {
            return R.layout.item_message_date;
        } else if (viewType == Type.ViewType.MESSAGE) {
            return R.layout.item_message_content;
        }
        try {
            throw new Exception("Unsupported view type");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (viewType) {
            case R.layout.item_message_content: {
                View view = inflater.inflate(R.layout.item_message_content, parent, false);
                viewHolder = new MessageViewHolder(view, this);
                break;
            }
            case R.layout.item_message_date: {
                View view = inflater.inflate(R.layout.item_message_date, parent, false);
                viewHolder = new DateViewHolder(view);
                break;
            }
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case R.layout.item_message_content: {
                MessageViewHolder viewHolder = (MessageViewHolder) holder;
                MessageContent messageContent = (MessageContent) dataset.get(position);
                viewHolder.tvMessageSender.setText(messageContent.sender);
                viewHolder.tvMessageTime.setText(messageContent.time);
                viewHolder.btnMarkAsSpam.setTextColor(mContext.getResources().getColor(messageContent.isSpam ? R.color.message_spam_color : R.color.message_not_spam_color));
                viewHolder.btnMarkAsSpam.setTag(position);
                viewHolder.tvMessageContent.setText(messageContent.content);
                break;
            }

            case R.layout.item_message_date: {
                DateViewHolder viewHolder = (DateViewHolder) holder;
                MessageDate messageDate = (MessageDate) dataset.get(position);
                viewHolder.tvDate.setText(messageDate.date);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_message_mark_spam: {
                int position = (int) v.getTag();
                MessageContent messageContent = (MessageContent) dataset.get(position);
                messageContent.isSpam = !messageContent.isSpam;
                messageController.toggleMessageAsSpamAsync(mContext, messageContent.id);
                ((Button) v).setTextColor(mContext.getResources().getColor(messageContent.isSpam ? R.color.message_spam_color : R.color.message_not_spam_color));
                MESSAGES_MARKED_AS_SPAM_COUNT = messageContent.isSpam ? MESSAGES_MARKED_AS_SPAM_COUNT + 1 : MESSAGES_MARKED_AS_SPAM_COUNT - 1;
                break;
            }
        }
    }
}
