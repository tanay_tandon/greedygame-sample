package com.greedygame.messageviewer.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.greedygame.messageviewer.R;
import com.greedygame.messageviewer.config.constants.Key;
import com.greedygame.messageviewer.view.adapter.MessageAdapter;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class DeletionConfirmationDialog extends DialogFragment implements View.OnClickListener{

    public static DeletionConfirmationDialog newInstance() {
        DeletionConfirmationDialog deletionConfirmationDialog = new DeletionConfirmationDialog();
        return deletionConfirmationDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_deletion_confirmation, container, false);
        getDialog().setTitle(R.string.confirmation_dialog_heading);
        String message = "Do you want to delete " + MessageAdapter.MESSAGES_MARKED_AS_SPAM_COUNT + " messages marked as spam?";
        ((TextView) view.findViewById(R.id.tv_deletion_message)).setText(message);
        view.findViewById(R.id.btn_delete_yes).setOnClickListener(this);
        view.findViewById(R.id.btn_delete_no).setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_delete_yes: {
                Intent intent = new Intent(getActivity().getResources().getString(R.string.intent_filter_deletion_broadcast));
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                getDialog().dismiss();
                break;
            }
            case R.id.btn_delete_no: {
                getDialog().dismiss();
                break;
            }
        }
    }
}
