package com.greedygame.messageviewer.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.greedygame.messageviewer.R;

/**
 * Created by tanaytandon on 15/01/16.
 */
public class DateViewHolder extends RecyclerView.ViewHolder {

    public TextView tvDate;

    public DateViewHolder(View view) {
        super(view);
        tvDate = (TextView) view.findViewById(R.id.tv_date_header);
    }
}
