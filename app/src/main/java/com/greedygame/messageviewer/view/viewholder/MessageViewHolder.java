package com.greedygame.messageviewer.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.greedygame.messageviewer.R;

/**
 * Created by tanaytandon on 14/01/16.
 */
public class MessageViewHolder extends RecyclerView.ViewHolder {

    public TextView tvMessageSender;
    public TextView tvMessageTime;
    public Button btnMarkAsSpam;
    public TextView tvMessageContent;

    public MessageViewHolder(View view, View.OnClickListener btnMarkAsSpamClickListener) {
        super(view);
        this.tvMessageSender = (TextView) view.findViewById(R.id.tv_message_sender);
        this.tvMessageTime = (TextView) view.findViewById(R.id.tv_message_time);
        this.btnMarkAsSpam = (Button) view.findViewById(R.id.btn_message_mark_spam);
        this.btnMarkAsSpam.setOnClickListener(btnMarkAsSpamClickListener);
        this.tvMessageContent = (TextView) view.findViewById(R.id.tv_message_content);
    }
}
