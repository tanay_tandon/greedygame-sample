package com.greedygame.messageviewer;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MessageDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "com.greedygame.messageviewer.model.dao");

        Entity message = schema.addEntity("Message");
        message.addIdProperty();
        message.addBooleanProperty("isDeleted");
        message.addBooleanProperty("isSpam");

        new DaoGenerator().generateAll(schema, "../app/src/main/java");;
    }
}
